/* Hello World program */

#include <config.h>
#include <stdio.h>

#ifndef VERSION
#define VERSION "No version"
#endif

int main(int args, char *argv[])
 {
     printf("Hello World, version: %s\n", VERSION);
     puts ("This is " PACKAGE_STRING ".");
  int i = 0;
 for (i = 0; i < args; i++)
 printf("\narg[%d]:%s",i, argv[i]);
 return 0;
}
